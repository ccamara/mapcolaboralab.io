---
title: Sobre nosotros
subtitle: Conoce qué hacemos y por qué lo hacemos
comments: false
---

Mapeado Colaborativo es uno de los [grupos residentes](https://www.zaragoza.es/ciudad/sectores/activa/grupos-residentes.htm) en [Zaragoza Activa](http://blogzac.es/) que nace en marzo de 2016 como grupo de investigación/acción para la innovación ciudadana y la canalización de la inteligencia colectiva. Su fin es  la creación de mapas colaborativos al servicio de la ciudadanía, a través de la concienciación sobre la importancia de la información geográfica en nuestro día a día y la promoción de herramientas y conocimientos para que colectivos y personas tengan la capacidad de realizar procesos de generación de información geográfica voluntaria y participativa.

### ¿Por qué es importante el mapeado colaborativo?

Explicar

### Cuando y dónde nos reunimos

Explicar e insertar un mapa embebido.

### ¿Qué hacemos?
Enlace a Proyectos. Quizá estaría bien insertar un listado automático que se actualice al añadir nuevos proyectos.
